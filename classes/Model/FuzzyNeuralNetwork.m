classdef FuzzyNeuralNetwork < Model
    
    %A Fuzzy Neural Network (FNN) learning model.
    
    properties
        % Structure containing the FIS
        fis;
        
        % Weights connecting to the hidden layer
        outputWeights;
        
        % Weights of the membership functions
        membershipWeights;
    end
    
    methods
        function obj = FuzzyNeuralNetwork(id, name, varargin)
            obj = obj@Model(id, name, varargin{:});
            obj.fis = [];
            obj.outputWeights = [];
            obj.membershipWeights = [];
        end
        
        function a = getDefaultTrainingAlgorithm(obj)
            a = RegularizedRWFNN(obj);
        end
        
        function p = initParameters(obj, p)
        end
        
        function [labels, scores] = test(obj, d)
            
            % Get training data
            Xts = d.X;
            H_temp_test = obj.computeHiddenMatrix(Xts);
            scores =(H_temp_test * obj.outputWeights);
            labels = convert_scores(scores, d.task);
            
        end
        
        function res = isDatasetAllowed(~, d)
            res = d.task == Tasks.R || d.task == Tasks.BC || d.task == Tasks.MC;
            res = res && d.X.id == DataTypes.REAL_MATRIX;
        end
        
%         function obj = setFis(obj, fisStructure)
%             obj.fis = fisStructure;
%         end

        function alpha = getMembershipWeights(obj)
            
            alpha = [];
            for i = obj.fis.input
                for j = i.mf
                    alpha = [alpha, j.params]; %Extract the antecedent parameters from the fis
                end
            end
            
        end
        
        function H = computeHiddenMatrix(obj, X)
            
            [N, d] = size(X);
            nRules = size(obj.fis.rule, 2);
            
            a = zeros(nRules, N, d);
            
            for ii = 1:nRules
                for jj = 1:d
                    mf = obj.fis.input(jj).mf(obj.fis.rule(ii).antecedent(jj));
                    a(ii, :, jj) = evalmf(X(:, jj), mf.params, mf.type);
                end
            end
            
            w = prod(a, 3); %Computes the unnormalized firing strengths
            w_hat = w./(repmat(sum(w, 1), nRules, 1)); %Normalizes the firing strengths
            w_hat(find(isnan(w_hat))) = 1/nRules; 
            H = [];
            for c = 1:size(w_hat, 1)
                H = [H repmat(w_hat(c, :)', 1, d+1).*[ones(N, 1) X]]; %Computes the hidden matrix
            end
        end
    end
    
    methods(Static)
        
        function info = getDescription()
            info = ['Fuzzy Neural Network, for more information, ' ...
                'please refer to the following paper: '];
        end
        
        function pNames = getParametersNames()
            pNames = {};
        end
        
        function pInfo = getParametersDescription()
            pInfo = {};
        end
        
        function pRange = getParametersRange()
            pRange = {};
        end
    end
    
end

