classdef RegularizedRWFNN < LearningAlgorithm
    
    % A least squares algorithm for Random-Weight Fuzzy Neural Networks.
    % The antecedent parameters are fixed, and the consequent parameters are
    % estimated using the LS algorithm.
    
    properties
    end
    
    methods
        
        function obj = RegularizedRWFNN(model, varargin)
            obj = obj@LearningAlgorithm(model, varargin{:});
        end
        
        function p = initParameters(~, p)
            p.addParamValue('lambda', 0, @(x) assert(x >= 0, 'Regularization parameter of RegularizedRWFNN must be >= 0'));
        end
        
        function obj = train(obj, dataset)
            
            % Get training data
            Xtr = dataset.X;
            Ytr = dataset.Y;
            
            [~, d] = size(Xtr);
            
            if(dataset.task == Tasks.MC)
                Ytr  = dummyvar(Ytr);
            end
            
            H = obj.model.computeHiddenMatrix(Xtr);
            [~, p] = size(H);
            
            boptimal = (H'*H + obj.parameters.lambda*eye(p)) \ (H'*Ytr);
            
            for i = 1:size(obj.model.fis.output.mf, 2)
                obj.model.fis.output.mf(i).params = boptimal((i - 1)*(d + 1) + 1: i*(d + 1))';
            end
            
            obj.model.outputWeights = boptimal;
        end
        
        function b = checkForCompatibility(~, model)
            b = model.isOfClass('FuzzyNeuralNetwork');
        end

        function b = hasGPUSupport(obj)
            b = false;
        end
        
        function obj = setFis(obj, fisStructure)
            obj.model.fis = fisStructure;
        end
    end
    
    methods(Static)

        function info = getDescription()
            info = ['Fuzzy Neural Network trained fixing the membership weights to random values and using L2-regularized ridge regression on the output weights. For more information, ' ...
                'please refer to the following paper:'];
        end
        
        function pNames = getParametersNames()
            pNames = {'lambda'}; 
        end
        
        function pInfo = getParametersDescription()
            pInfo = {'Regularization factor'};
        end
        
        function pRange = getParametersRange()
            pRange = {'Positive real number, default is 0'};
        end    
    end
end