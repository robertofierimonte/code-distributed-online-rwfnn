classdef OnlineSequentialRWFNN < SequentialLearningAlgorithm
    % BRLS - Blockwise Recursive Least Squares algorithm for a Fuzzy Neural
    % Network. This algorithm recursively updates the parameters of the
    % consequents when a new chunk of data arrives.
    
    properties
        P;        % Internal Matrix
    end
    
    methods
        
        function obj = OnlineSequentialRWFNN(model, varargin)
            obj = obj@SequentialLearningAlgorithm(model, varargin{:});
        end
        
        function p = initParameters(obj, p)
            p.addParamValue('lambda', 0, @(x) assert(x >= 0, 'Regularization parameter of RegularizedRWFNN must be > 0'));
            p = obj.initParameters@SequentialLearningAlgorithm(p);
        end
        
        function obj = train_init(obj, dataset_init)
            
            % Get training data
            X0 = dataset_init.X;
            Y0 = dataset_init.Y;
            
            [N, d] = size(X0);
            [~, m] = size(obj.model.fis.rule);
            
            % Initialize the internal Matrix and the weights of the model
            obj.P = eye(m*(d+1))/obj.parameters.lambda;
            obj.model.outputWeights = zeros(m*(d+1), 1);
            
            if(dataset_init.task == Tasks.MC)
                Y0 = dummyvar(Y0(:));
            end
            
            H0 = obj.model.computeHiddenMatrix(X0);
            M = eye(N) + H0 * obj.P * H0';
            
            obj.P = obj.P - obj.P * H0' * M^(-1) * H0 * obj.P;
            
            obj.model.outputWeights = obj.model.outputWeights + obj.P * H0' * (Y0 - H0 * obj.model.outputWeights);
            
        end
        
        function obj = train_step(obj, dataset_batch)
            
            % Get training data
            Xn = dataset_batch.X;
            Yn = dataset_batch.Y;
            [N, d] = size(Xn);
            
            if(dataset_batch.task == Tasks.MC)
                Yn = dummyvar(Yn(:));
            end
                
            H = obj.model.computeHiddenMatrix(Xn);
            M = eye(N) + H * obj.P * H';
                
            obj.P = obj.P - obj.P * H' * M^(-1) * H * obj.P;
            obj.model.outputWeights = obj.model.outputWeights + obj.P * H' * (Yn - H * obj.model.outputWeights);
            
%             for i = 1:size(obj.model.fis.output.mf, 2)
%                 obj.model.fis.output.mf(i).params = newBeta((i - 1)*(d + 1) + 1: i*(d + 1))';
%             end            
        end
        
        function b = checkForCompatibility(~, model)
            b = model.isOfClass('FuzzyNeuralNetwork');
        end
        
        function obj = setFis(obj, fisStructure)
            obj.model.fis = fisStructure;
        end
    end
    
    methods(Static)
        function info = getDescription()
            info = 'Online Sequential Random-Weight Fuzzy Neural Network trained using BRLS. For more information, please refer to the following paper: ';
        end
        
        function pNames = getParametersNames()
            pNames = SequentialLearningAlgorithm.getParametersNames();
        end
        
        function pInfo = getParametersDescription()
            pInfo = SequentialLearningAlgorithm.getParametersDescription();
        end
        
        function pRange = getParametersRange()
            pRange = SequentialLearningAlgorithm.getParametersRange();
        end
    end
end

