classdef HybridTrainingAlgorithm
    
    properties
    end
    
    methods
        function [fnn, info] = solve(~, fnn, trainData)
            
            info = struct();
            
            Xtr = trainData.X;
            Ytr = trainData.Y;
            
            tic;
            fnn.fis = anfis([Xtr, Ytr], fnn.fis);
            info.trainTime = toc;
        end
    end
    
end

