classdef SerialDataDistributedOnlineRWFNN < DistributedLearningAlgorithm
    
    %
    
    properties
        beta;
        P;
        models_history;
    end
    
    methods
        function obj = SerialDataDistributedOnlineRWFNN(model, varargin)
            obj = obj@DistributedLearningAlgorithm(model, varargin{:});
            obj.distribute_data = true;
        end
        
        function p = initParameters(~, p)
            p.addParamValue('lambda', 1, @(x) assert(x > 0, 'Regularization parameter of SerialDataDistributedRWFNN must be > 0'));
            p.addParamValue('train_algo', 'consensus', @(x) assert(isingroup(x, {'consensus'}), ...
                'Lynx:Runtime:Validation', 'The train_algo of SerialDataDistributedRVFL can be: consensus, admm'));
            p.addParamValue('consensus_max_steps', 300);
            p.addParamValue('consensus_thres', 0.001);
            p.addParamValue('N0', 15, @(x) assert(isnatural(x, false), 'Initial block size of a sequential algorithm must be a non-zero natural number'));
            p.addParamValue('blockSize', 15, @(x) assert(isnatural(x, false), 'Block size of a sequential algorithm must be a non-zero natural number'));
            
        end
        
        function obj = train_locally(obj, dataset)     
            
            N_hidden = size(obj.model.fis.rule,2)*(size(dataset.X,2)+1);
            N_nodes = obj.topology.N;
            
            obj.P = cell(N_nodes, 1);
            
            is_multiclass = dataset.task == Tasks.MC;

            if(is_multiclass)
                nLabels = max(dataset.Y);
                beta = rand(N_hidden, nLabels, N_nodes);
                % Strange cell array to index conditionally either the 
                % first 2 dimensions or only the first.
                idx = {':', ':'};
            else
                nLabels = 1;
                beta = rand(N_hidden, N_nodes);
                idx = {':'};
            end

            if(strcmp(obj.getParameter('train_algo'), 'consensus'))
            
                dataset_init = cell(N_nodes, 1);
                dataset_rest = cell(N_nodes, 1);
                n_batches = zeros(N_nodes,1);

                for ii = 1:N_nodes

                    d_local = dataset.getLocalPart(ii);

                    % Compute the number of batches
                    size_afterinit = size(d_local.X, 1) - obj.parameters.N0;
                    n_batches(ii) = floor(size_afterinit/obj.parameters.blockSize);

                    % Get the initial batch
                    d_local = d_local.generateSinglePartition(ExactPartition(obj.parameters.N0, size_afterinit));
                    [dataset_init{ii}, dataset_rest{ii}] = d_local.getFold(1);
                end
                
                obj.models_history = cell(max(n_batches + 1), 1);
                obj.beta = cell(max(n_batches + 1), 1);
                %obj.error_history = cell(max(n_batches + 1), 1);
                %fprintf('Number of Batch Iterations: %i\n', max(n_batches));
                 
                for ii = 1:N_nodes

                    % Get training data
                    X0 = dataset_init{ii}.X;
                    Y0 = dataset_init{ii}.Y;

                    [N, d] = size(X0);
                    [~, m] = size(obj.model.fis.rule);

                    % Initialize the internal Matrix
                    obj.P{ii} = eye(m*(d+1))/obj.parameters.lambda;

                    if(dataset_init{ii}.task == Tasks.MC)
                        Y0 = dummyvar(Y0(:));
                    end

                    H0 = obj.model.computeHiddenMatrix(X0);
                    M = eye(N) + H0 * obj.P{ii} * H0';

                    obj.P{ii} = obj.P{ii} - obj.P{ii} * H0' * M^(-1) * H0 * obj.P{ii};

                    out = beta(idx{:},ii);

                    out = out + obj.P{ii} * H0' * (Y0 - H0 * out);

                    beta(idx{:},ii) = out;
                end

                % Execute (serial) consensus algorithm
                if(obj.getParameter('consensus_max_steps') > 0)
                    betaCopy = beta;
                    [obj.model.outputWeights, obj.statistics.consensus_error] = ...
                        obj.run_consensus_serial(beta, obj.getParameter('consensus_max_steps'), obj.getParameter('consensus_thres'));

                    % sum(sqrt((obj.model.outputWeights-mean(betaCopy, 2)).^2))
                    % [beta, ~] = ...
                    %     obj.run_consensus_serial(betaCopy, obj.getParameter('consensus_max_steps'), obj.getParameter('consensus_thres'));
                else
                    obj.model.outputWeights = beta(idx{:}, 1);
                end
                %fprintf('Mini batch iteration 1\n');
                
                obj.models_history{1} = obj;
                obj.beta{1} = beta;

                if (max(n_batches) > 1)
                    for ii = 1:N_nodes
                        dataset_rest{ii} = dataset_rest{ii}.generateSinglePartition(KFoldPartition(n_batches(ii)));
                    end
                else
                    for ii = 1:N_nodes
                        dataset_rest{ii} = dataset_rest{ii}.generateSinglePartition(NoPartition());
                    end
                end

                for n = 1:max(n_batches)

                    for ii = 1:N_nodes
                        [~, dataset_batch, ~] = dataset_rest{ii}.getFold(n);

                        Xn = dataset_batch.X;
                        Yn = dataset_batch.Y;

                        [N, ~] = size(Xn);

                        if(dataset_batch.task == Tasks.MC)
                            Yn = dummyvar(Yn(:));
                        end

                        Hn = obj.model.computeHiddenMatrix(Xn);
                        M = eye(N) + Hn * obj.P{ii} * Hn';

                        obj.P{ii} = obj.P{ii} - obj.P{ii} * Hn' * M^(-1) * Hn * obj.P{ii};

                        out = obj.model.outputWeights + obj.P{ii} * Hn' * (Yn - Hn * obj.model.outputWeights);

                        beta(idx{:},ii) = out;
                    end

                    % Execute (serial) consensus algorithm
                    if(obj.getParameter('consensus_max_steps') > 0)
                        betaCopy = beta;
                        [obj.model.outputWeights, obj.statistics.consensus_error] = ...
                            obj.run_consensus_serial(beta, obj.getParameter('consensus_max_steps'), obj.getParameter('consensus_thres'));

                        % sum(sqrt((obj.model.outputWeights-mean(betaCopy, 2)).^2))
                        % [beta, ~] = ...
                        %     obj.run_consensus_serial(betaCopy, obj.getParameter('consensus_max_steps'), obj.getParameter('consensus_thres'));
                    else
                        obj.model.outputWeights = beta(idx{:}, 1);
                    end

                    obj.models_history{n+1} = obj;
                    obj.beta{n+1} = beta;
                    %fprintf('Mini batch iteration %i', n+1);
                end
            end
        end
        
        function obj = train(obj, dataset)
            
            if(isempty(obj.topology))
                error('Lynx:Runtime:MissingFeature', 'To use a distributed learning algorithm, please initialize the topology with the feature InitializeTopology');
            end

            % For distributing data in non-parallel mode, we use a
            % simple K-fold partition on the number of samples.
            fprintf('\t\tEach node will have approximately %i patterns.\n', floor(size(dataset.X, 1)/obj.topology.N));
            dataset = dataset.generateSinglePartition(KFoldPartition(obj.topology.N));
            
            obj = obj.executeBeforeTraining(dataset);
            
            obj.obj_locals{1} = obj.train_locally(dataset);
            
            obj = obj.executeAfterTraining();
        end
        
        function obj = executeBeforeTraining(obj, d)
        end
        
        function obj = executeAfterTraining(obj)
            % Average stats throughout the models, and select the model of
            % the first node for comparing the errors.
            stats = cell(length(obj.obj_locals), 1);
            for i = 1:length(obj.obj_locals)
                o = obj.obj_locals{i};
                stats{i} = o.statistics;
            end
            obj = obj.obj_locals{1};
            obj.statistics = sum_structs(stats);
        end
        
        function b = checkForCompatibility(~, model)
            b = model.isOfClass('FuzzyNeuralNetwork');
        end
        
        function obj = setFis(obj, fisStructure)
            obj.model.fis = fisStructure;
        end
%         
        function errors = compute_error_history(obj, dataset)
            % Utility function for computing the evolution of the testing
            % error (used in the info_sequential script).
            p = PerformanceEvaluator.getInstance();
            p = p.getPrimaryPerformanceMeasure(dataset.task);

            errors = zeros(length(obj.models_history), 1);
            dataset = dataset.generateSinglePartition(NoPartition());
            for ii = 1:length(obj.models_history)
                [labels, scores] = obj.models_history{ii}.test(dataset);
                errors(ii) = p.compute(dataset.Y, labels, scores);
            end
        end
        
%         % Initialize the algorithm
%         function obj = train_init(obj, dataset_init)
%         end
%         
%         % Training step
%         function obj = train_step(obj, dataset_batch)
%         end
    end
    
    methods(Static)

        function info = getDescription()
            info = ['Data-distributed Online RWFNN'];
        end
        
        function pNames = getParametersNames()
            pNames = {'lambda', 'train_algo', 'consensus_max_steps', 'consensus_thres'}; 
        end
        
        function pInfo = getParametersDescription()
            pInfo = {'Regularization factor', 'Training algorithm', 'Iterations of consensus', 'Threshold of consensus'};
        end
        
        function pRange = getParametersRange()
            pRange = {'Positive real number, default is 1', 'String in {consensus [default]}', 'Positive integer, default to 300', 'Positive real number, default to 0.001'};
        end    
    end
end

