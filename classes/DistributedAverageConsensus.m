classdef DistributedAverageConsensus
    
    % This is a serial implementation of the consensus strategy.
    % Parameters are:
    %
    %   - INITIAL_VALUES: matrix of initial values. For consensus
    %   of d-dimensional vectors, this is a dxN matrix, where N is
    %   the total number of agents.
    %
    %   - MAX_STEPS: maximum number of iterations
    %
    %   - THRESHOLD: threshold for the disagreement norm. When this
    %   is the lower than the threshold on all nodes, consensus is
    %   ended.
    %
    % Output values are the computed average, and the average
    % evolution of the disagreement on the nodes.
    
    properties
        
        maxSteps;
        threshold;
        
    end
    
    methods
        function obj = DistributedAverageConsensus(varargin)
            p = inputParser();
            p.addParameter('consensus_max_steps', 300);
            p.addParameter('consensus_thres', 0.001);
            p.parse(varargin{:});
            obj.threshold = p.Results.consensus_thres;
            obj.maxSteps = p.Results.consensus_max_steps;
        end
        
        function [final_value, consensus_error] = solve(obj, ~, net, ~, beta0)           
                        
            current_values = beta0;
            consensus_error = zeros(obj.maxSteps, 1);
            
            for ii = 1:obj.maxSteps
                
                current_values = current_values*net.W;
                aux = current_values - repmat(mean(current_values, 2), 1, net.N);
                consensus_error(ii) = mean(diag(sqrt(aux'*aux)));
                
                if(consensus_error(ii) < obj.threshold)
                    break;
                end
                
            end
            
            final_value = current_values(:, 1);
            
        end
    end
end