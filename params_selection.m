% -------------------------------------------------------------------------
% --- PARAMS_SELECTION ----------------------------------------------------
% --- Define the parameters for the simulation ----------------------------
% -------------------------------------------------------------------------

% Dataset configuration
datasetFile = 'airfoil.mat';   % Dataset to load
runs = 5;                   % Number of simulations
kfolds = 10;                % Number of folds

% Network configuration
agents = [5:5:10];            % Nodes in the network (can be a vector for testing multiple sizes simultaneously)
connectivity = 0.25;        % Connectivity in the networks (must be between 0 and 1)

load(datasetFile);
if strcmp(name, 'airfoil')
    
    dataset = Dataset(name, X(1:1500, :), task, Y(1:1500, :));    % Load and preprocess dataset Data
    dataset = normalize(dataset, -1, 1);
    lambda = 0.1;
    
elseif strcmp(name, 'CCPP')
    
    dataset = Dataset(name, X(1:9500, :), task, Y(1:9500, :));    % Load and preprocess dataset Data
    dataset = normalize(dataset, -1, 1);
    lambda = 0.01;
    
elseif strcmp(name, 'CASP')
    
    dataset = Dataset(name, X, task, Y);
    dataset = normalize(dataset, -1, 1);
    lambda = 0.001;
end