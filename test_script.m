%clear all; 
clc; rng(10);
f = genpath(pwd); addpath(f);
warning('off', 'MATLAB:nearlySingularMatrix');
echo anfis off;

% Preprocess dataset and select parameters for the simulation
params_selection;

% Split the dataset
dataset = dataset.generateNPartitions(runs, KFoldPartition(kfolds), NoPartition);

% Error functions for Regression
error_functions_reg = {...
	@(d,y) sqrt(norm((d - y).^2)/(length(d) * var(d))),
    @(d,y) 10*log10(sum((d - y).^2)/sum(d.^2)),
};

% Error functions for Classification
error_functions_class = {...
    @(d,y) sum(d~=y)/length(d),
};

% Define the centralized algorithms
centralized_models = {...
    FuzzyNeuralNetwork('C-RWFNN', 'C-RWFNN'),
    FuzzyNeuralNetwork('C-OL-RWFNN', 'C-OL-RWFNN'),
};

centralized_algorithms = {...
    RegularizedRWFNN(centralized_models{1}, 'lambda', 10),
    OnlineSequentialRWFNN(centralized_models{2}, 'lambda', 10, 'N0', 5, 'blockSize', 5),
};

% Define the distributed algorithms
distributed_models = {...
      FuzzyNeuralNetwork('D-RWFNN', 'D-RWFNN'),
      FuzzyNeuralNetwork('D-OL-RWFNN', 'D-OL-RWFNN'),
};

distributed_algorithms = {
    SerialDataDistributedRWFNN(distributed_models{1}, 'lambda', 10),
    SerialDataDistributedOnlineRWFNN(distributed_models{2}, 'lambda', 10, 'N0', 5, 'blockSize', 3),
};

% Define the local algorithms
local_algorithms = {...
%     FuzzyNeuralNetwork('', '', RegularizedRWFNN()),
%     FuzzyNeuralNetwork('', '', OnlineSequentialRWFNN()),
};

% -----------------------------
% --- MAIN SIMULATION ---------
% -----------------------------

% Initialize output matrices
if dataset.task == Tasks.R
    error_functions = error_functions_reg;
else
    error_functions = error_functions_class;
end

N_algorithms = length(centralized_models) + length(distributed_models)*length(agents)...
             + length(local_algorithms)*length(agents);
errors = zeros(runs*kfolds, N_algorithms, length(error_functions));
time = zeros(runs*kfolds, N_algorithms);

% Structures for saving internal evaluations of objective function and
% gradient norm
trainInfo = cell(runs*kfolds, N_algorithms);

% Initialize the networks' topologies
nets = cell(length(agents), 1);
for ii = 1:length(agents)
    nets{ii} = RandomTopology(agents(ii), 'metropolis', connectivity);
end

z = 1;  % Auxiliary index for the output structures

for n = 1:runs
    
    fprintf('--- RUN %i/%i ---\n', n, runs);
    
    % Set the current partition in the dataset
    dataset = dataset.setCurrentPartition(n);
    
    fis = random_fis(dataset.X, dataset.Y, 2, 'gaussmf');
    
    for k = 1:kfolds
       
        % Split and distribute the data
        [trainData, testData, ~] = dataset.getFold(k);
        trainDataDistributed = cell(length(agents), 1);
        for ii = 1:length(agents)
            if(agents(ii) > 1)
                distrPartition = KFoldPartition(agents(ii));
            else
                distrPartition = NoPartition();
            end
            trainDataDistributed{ii} = trainData.distributeDataset(distrPartition);
        end      
        
        fprintf('\tFold %i/%i - %i training, %i test\n', k, kfolds, size(trainData.X, 1), size(testData.X, 1));
        
        % Test the centralized algorithms
        for a = 1:length(centralized_models)
            fprintf('\t\tTraining %s...\n', centralized_models{a}.name);
            if centralized_models{a}.isOfClass('FuzzyNeuralNetwork')
                centralized_algorithms{a} = centralized_algorithms{a}.setFis(fis);
            end       
            tic;
            centralized_algorithms{a} = centralized_algorithms{a}.train(trainData);
            time(z, a) = toc;
            for e = 1:length(error_functions)
                [labels, ~] = centralized_algorithms{a}.test(testData);
                errors(z, a, e) = error_functions{e}(testData.Y, labels);
            end
        end
        
        % Correct for the index
        if isempty(centralized_algorithms)
            a = 0;
        end
        a = a + 1;
        
        % Test the distributed algorithms
        for b = 1:length(distributed_models)
            if distributed_models{b}.isOfClass('FuzzyNeuralNetwork')
                distributed_algorithms{b} = distributed_algorithms{b}.setFis(fis);
            end
            for ii = 1:length(agents)
                L = agents(ii);
                fprintf('\t\tTraining %s (%i agents)...\n', distributed_algorithms{b}.name, L);
                tic;
                distributed_algorithms{b} = distributed_algorithms{b}.setTopology(nets{ii});
                distributed_algorithms{b} = distributed_algorithms{b}.train(trainDataDistributed{ii});
                time(z, a) = toc./L;
                for e = 1:length(error_functions)
                    [labels, ~] = distributed_algorithms{b}.test(testData);
                    errors(z, a, e) = error_functions{e}(testData.Y, labels);
                end
                a = a + 1;
            end
        end
        
        % test for the local algorithms       
        
        for c = 1:length(local_algorithms)
            local_algorithms{c}.setFis(fis);
            for ii = 1:length(agents)
                L = agents(ii);
                fprintf('\t\tTraining %s (%i agents)...\n', local_algorithms{c}.name, L);
                tempError = zeros(length(error_functions));
                tempTime = 0;
                for jj = 1:L
                    tic;
                    [local_algorithms{c}, trainInfo{z, a}] = ...
                        local_algorithms{c}.train(trainDataDistributed{ii}.getLocalPart(jj));
                    tempTime = tempTime + toc;
                    for e = 1:length(error_functions)
                        tempError(e) = tempError(e) + error_functions{e}(testData.Y, local_algorithms{c}.test(testData));
                    end
                end
                time(z, a) = tempTime./L;
                for e = 1:length(error_functions)
                    errors(z, a, e) = tempError(e)./L;
                end
                a = a + 1;
            end
        end
        
        z = z + 1;
        
    end
    
end

%% -----------------------------
% --- OUTPUT-------------------
% -----------------------------

fprintf('-----------------------\n');
fprintf('--- RESULTS -----------\n');
fprintf('-----------------------\n');

% Collect the names of the algorithms (in the output, there is one
% algorithm for each tested size of the network)
names = cell(N_algorithms, 1);
for i=1:length(centralized_algorithms)
    names{i} = centralized_algorithms{i}.name;
end

if(isempty(i))
    i = 1;
else
    i = i + 1;
end

for j=1:length(distributed_algorithms)
    for z=1:length(agents)
        names{i} = [distributed_algorithms{j}.name, ' (', num2str(agents(z)), ')'];
        i = i + 1;
    end
end

for h = 1:length(local_algorithms)
    for z = 1:length(agents)
        names{i} = [local_algorithms{h}.name, '(', num2str(agents(z)), ')'];
        i = i + 1;
    end
end

% Show the output tables and plots
disptable([mean(errors(:, :, 1), 1)' std(errors(:, :, 1), 1)' mean(errors(:, :, 2),1)' std(errors(:, :, 2), 1)' mean(time,1)' std(time, 1)'], {'NRMSE', '(S.D.)', 'SNR', '(S.D.)', 'Tr. times', '(S.D.)'}, names); 
% make_plots;